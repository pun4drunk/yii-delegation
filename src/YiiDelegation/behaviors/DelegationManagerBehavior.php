<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VirtualProxyManagerBehavior
 *
 * @author vladislav
 */

namespace YiiDelegation\behaviors;

use YiiComponents\behaviors\DependentBehavior;
use YiiComponents\helpers\ArrayHelper;
use YiiDelegation\interfaces\IDelegationManager;

class DelegationManagerBehavior extends DependentBehavior implements IDelegationManager {
    
    public $overrideParams = array();
    public $objectMap = array();
    public $params = array();
    protected $paramsKey = 'params';

    protected $_objects = array();
    
    public function run($action, $args = array()) {
        $objectName = $this->getObjectName($action);
        $this->attachObject($objectName);
        
        return call_user_func_array(array($this->$objectName, $this->getMethodName($action)), $args);
    }
    
    public function getMethodName($action) {
        return $action;
    }
    
    public function getParam($name, $action, $default = NULL, $autoAttach = false) {
        
        $objectName = $this->getObjectName($action);
        if (!isset($this->_objects[$objectName])) {
            
            if ($autoAttach) $this->attachObject($objectName);
            else throw new \CException("Delegate $objectName is not attached yet");
            
        }
        
        $params = ArrayHelper::get($action, $this->_objects[$objectName][$this->paramsKey], array());
        
        return ArrayHelper::get($name, $params, $default);
    }
    
    public function getObjectName($action) {
        return $this->objectMap[$action];
    }
    
    public function getParams($objectName) {
        return $this->params[$objectName];
    }

    public function attachObject($name) {
        
        if (!isset($this->_objects[$name])) {
            
            $config = $this->getParams($name);

            if (isset($config[$this->paramsKey])) {
                $config[$this->paramsKey] = $this->processParams($config[$this->paramsKey]);
            }

            $this->_attachBehavior($name, $config);
            $this->_objects[$name] = $config;
        }
        
        return true;
    }
    
    protected function processParams($params) {
        return $params;
    }
    
    //children method can use this proxy to add necessary logic
    protected function _attachBehavior($name, $config) {
        $this->attachBehavior($name, $config);
        return $this;
    }
    
}
