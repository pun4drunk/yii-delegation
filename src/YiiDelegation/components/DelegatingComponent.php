<?php

/*
 * @category  Projects
 * @package   self.socialFella.reborn
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of DelegatingComponent
 *
 * @author vladislav
 */
namespace YiiDelegation\components;
use CApplicationComponent;

abstract class DelegatingComponent extends CApplicationComponent {
    
    protected $delegate;
    
    public function __call($name, $parameters) {
        if (method_exists($this->delegate, $name)) {
            return call_user_func_array(array($this->delegate, $name), $parameters);
        }
        parent::__call($name, $parameters);
    }
}
