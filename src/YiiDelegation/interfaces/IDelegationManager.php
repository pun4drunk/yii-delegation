<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of IScenariosWorker
 *
 * @author vladislav
 */
namespace YiiDelegation\interfaces;

interface IDelegationManager {
    
    public function run($method, $args = array());
    public function getParam($name, $action, $default = NULL);
    public function getParams($objectName);
    public function getObjectName($action);
    public function getMethodName($action);
    public function attachObject($objectName);

}
